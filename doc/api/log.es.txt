Las versiones antiguas del ircu no tenían muy buenos medios para lidiar
con el registro. En la versión u2.10.11, se escribió un subsistema de
registro completamente nuevo, que le da a un administrador del servidor
mucha más capacidad para determinar qué acciones se deben registrar y
donde. El nuevo subsistema de registro permite que los mensajes de
registro vayan a syslog, a un archivo y a los operadores del servidor a
través de avisos del servidor, simultáneamente (aunque actualmente no se
admite la salida a varios archivos de registro).

Todos los mensajes de registro tienen dos valores que se pasan con ellos:
el nivel de registro, que debe ser uno de los valores del enumerador LogLevel,
y un subsistema de registro, que debe ser uno de los valores de LogSys;
estos valores se utilizan como índices en matrices dentro de ircd_log.c,
así que tenga cuidado si los cambia.

Además de LogLevel y LogSys, también hay un conjunto de tres indicadores
(flags) que se pueden pasar a la función de registro log_write(); estos
indicadores se pueden usar para suprimir ciertos tipos de registro que
pueden ser indeseables. Por ejemplo, cuando un servidor enlaza, se puede
escribir un registro que contiene la dirección IP del servidor; para evitar
que esta dirección IP aparezca en un aviso del servidor, a esa invocación
de log_write() se le pasa el indicador LOG_NOSNOTICE.

<enum>
enum LogLevel {
  L_CRIT,
  L_ERROR,
  L_WARNING,
  L_NOTICE,
  L_TRACE,
  L_INFO,
  L_DEBUG,
  L_LAST_LEVEL
};

Esta enumeración describe los niveles de gravedad de un mensaje de registro.
La gravedad disminuye a medida que avanza hacia abajo en la lista, por lo cual
L_DEBUG es menos grave que L_INFO y L_CRIT en el más grave de todos. El valor
especial L_LAST_LEVEL nunca debe usarse; simplemente marca el final de la lista.
</enum>

<enum>
enum LogSys {
  LS_SYSTEM, LS_CONFIG, LS_OPERMODE, LS_GLINE, LS_JUPE, LS_WHO, LS_NETWORK,
  LS_OPERKILL, LS_SERVKILL, LS_USER, LS_OPER, LS_RESOLVER, LS_SOCKET,
  LS_DEBUG, LS_OLDLOG,
  LS_LAST_SYSTEM
};

Estos son los diversos subsistemas de registro reconocidos por el subsistema
de registro. Nuevamente, el orden es importante y, nuevamente, LS_LAST_SYSTEM 
nunca debe usarse.
</enum>

<function>
void log_debug_init(int usetty);

Esto inicializa el código de registro de depuración de propósito
especial en el servidor. Si el parámetro _usetty_ no es cero, toda la
salida de depuración irá al terminal, independientemente de la configuración
de archivos para el subsistema LS_DEBUG. Esta función no está definida a
menos que el servidor se compile con -DDEBUGMODE.
</function>

<function>
void log_init(const char *process_name);

Esto inicializa todo el subsistema de registro, incluyendo las cosas
especiales como almacenar el nombre del proceso y abrir syslog con la
función open_log (). Solo se puede llamar una vez.
</function>

<function>
void log_reopen(void);

Todos los archivos de registro se abren persistentemente, para evitar
la sobrecarga de volver a abrir el archivo de registro cada vez. Esta
función se utiliza para cerrar todos los archivos de registro y para
cerrar y volver a abrir syslog. (Los archivos de registro se abren de
nuevo solo cuando hay algo que escribir en ellos).
</function>

<function>
void log_close(void);

Esto cierra todos los archivos de registro y el syslog antes de que
finalice el servidor. Si es necesario volver a abrir los registros
después de llamar a esta función, llame a log_reopen() en lugar de
log_init().
</function>

<function>
void log_write(enum LogSys subsys, enum LogLevel severity,
	       unsigned int flags, const char *fmt, ...);

Esta es la actual función de registro. El parámetro _flags_ es 0 o
el bit OR de LOG_NOSYSLOG (suprime syslogging), LOG_NOFILELOG
(suprime el registro en un archivo) y LOG_NOSNOTICE (suprime el
registro mediante avisos del servidor). El parámetro _fmt_ es una
cadena de formato aceptable para ircd_snprintf(), que es la función
llamada para formatear realmente el mensaje de registro.
</function>

<function>
void log_vwrite(enum LogSys subsys, enum LogLevel severity,
		unsigned int flags, const char *fmt, va_list vl);

Esto es similar a log_write() excepto que toma un parámetro va_list.
</function>

<function>
char *log_cannon(const char *subsys);

Esto devuelve el nombre canónico para el subsistema de registro.
Probablemente, esto no debería ser expuesto aquí, pero es necesario
en ircd_features.c actualmente.
</function>

<function>
int log_set_file(const char *subsys, const char *filename);

Esto establece el nombre de archivo para el subsistema de registro
especificado en _filename_; devuelve 2 si el subsistema no estaba
definido, 1 si no se ha reconocido el valor de _filename_ o 0 si no
hubo error.
</function>

<function>
char *log_get_file(const char *subsys);

Esto devuelve el nombre del archivo de registro actual para el
subsistema dado.
</function>

<function>
int log_set_facility(const char *subsys, const char *facility);

Esto establece la facilidad de syslog para el subsistema de registro
especificado en _facility_; devuelve 2 si el subsistema no estaba
definido, 1 si no se ha reconocido el valor de _facility_ o 0 si no
hubo error. Se pueden dar dos nombres de instalaciones especiales;
"NONE" especifica que no se debe realizar syslogging, y "DEFAULT" 
especifica que se debe usar la función de syslog predeterminada del
ircd.
</function>

<function>
char *log_get_facility(const char *subsys);

Esto devuelve la facilidad actual de syslog para el subsistema dado.
Consulte la documentación de log_set_facility() para obtener una
descripción de los nombres de las falicidades especiales "NONE" y
"DEFAULT".
</function>

<function>
int log_set_snomask(const char *subsys, const char *snomask);


Esto establece el tipo de aviso del servidor para el subsistema de
registro especificado en _snomask_; devuelve 2 si el subsistema no
estaba definido, 1 si no se ha reconocido el valor de _snomask_ o
0 si no hubo error. El tipo de aviso especial del servidor "NONE"
indica que no se deben generar avisos del servidor. Los otros valores
válidos para _snomask_ son:
"OLDSNO," "SERVKILL," "OPERKILL," "HACK2," "HACK3," "UNAUTH,"
"TCPCOMMON," "TOOMANY," "HACK4," "GLINE," "NETWORK," "IPMISMATCH,"
"THROTTLE," "OLDREALOP," "CONNEXIT," y "DEBUG."
</function>

<function>
char *log_get_snomask(const char *subsys);

Esto devuelve el tipo actual de aviso del servidor para el subsistema
dado. Consulte la documentación de log_set_snomask() para obtener una
descripción de los valores de retorno.
</function>

<function>
int log_set_level(const char *subsys, const char *level);

Esta función se utiliza para establecer el nivel mínimo de registro para
un subsistema particular; devuelve 2 si el subsistema no estaba definido,
1 si no se ha reconocido el valor de _level_ o 0 si no hubo error. No se
registrarán los avisos de registro generados con una gravedad inferior a
la establecida con esta función. Los valores válidos son "CRIT", "ERROR",
"WARNING", "NOTICE", "TRACE", "INFO" y "DEBUG".
</function>

<function>
char *log_get_level(const char *subsys);

Esto devuelve el nivel actual de registro mínimo para el subsistema
dado. Consulte la documentación de log_set_level() para obtener una
descripción de los valores de retorno.
</function>

<function>
int log_set_default(const char *facility);

This function sets the default syslog facility for all of ircd.  Valid
values for _facility_ are as described for log_set_facility() with the
exclusion of the "NONE" and "DEFAULT" facilities; returns 1 if the
facility name was unrecognized (or proscribed) or 0 if there was no
error.
Esta función establece la facilidad predeterminada de syslog para todos
los ircd. Los valores válidos para _facility_ son los descritos para
log_set_facility() con la exclusión de las facilidades "NONE" y "DEFAULT";
devuelve 1 si el nombre de la facilidad no fue reconocido (o proscrito)
o 0 si no hubo error.
</function>

<function>
char *log_get_default(void);

Esto simplemente devuelve la facilidad de syslog por defecto de ircd.
</function>

<function>
void log_feature_unmark(void);

Esta función es llamada por el subsistema ircd_features.c y no debe
ser llamada por ninguna otra parte del ircd. Consulte la documentación
de la API de características para obtener notas sobre lo que hace esta
función.
</function>

<function>
void log_feature_mark(int flag);

Esta función es llamada por el subsistema ircd_features.c y no debe
ser llamada por ninguna otra parte del ircd. Consulte la documentación
de la API de características para obtener notas sobre lo que hace esta
función.
</function>

<function>
void log_feature_report(struct Client *to, int flag);

Esta función es llamada por el subsistema ircd_features.c y no debe
ser llamada por ninguna otra parte del ircd. Consulte la documentación
de la API de características para obtener notas sobre lo que hace esta
función.
</function>

<authors>
Kev <klmitch@mit.edu> Original
zoltan <toni@tonigarcia.es> Traducción
</authors>

<changelog>
[2001-06-13 Kev] Corregir un error tipográfico menor

[2000-12-18 Kev] Escribió alguna documentación sobre
cómo usar el subsistema de registro.
</changelog>