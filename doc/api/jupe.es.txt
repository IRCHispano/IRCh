Ocasionalmente, un servidor se configurará incorrectamente o comenzará
a comportarse incorrectamente. Aún más raramente, un servidor se verá
comprometido y se implementará una versión modificada del servidor para
causar problemas. Estos casos son la razón de la _jupe_, una prohibición
temporal del servidor. Esto es similar a la G-line, que es una prohibición
temporal del usuario, y de hecho, la API jupe es muy similar a la API de la
G-line.

<macro>
#define JUPE_MAX_EXPIRE	604800	/* máxima expiración: 7 días */

Esta macro enumera el tiempo máximo de caducidad que puede tener un jupe.
Este valor está limitado a 7 días para evitar el abuso del sistema.
</macro>

<macro>
#define JUPE_ACTIVE	0x0001

Este indicador (flag) se utiliza para indicar que un jupe determinado está
activo globalmente.
</macro>

<macro>
#define JUPE_LOCAL	0x0002

Este indicador se usa para indicar que un jupe dado es local. Los jupes
locales no afectan a los usuarios en otros servidores.
</macro>

<macro>
#define JUPE_LDEACT	0x0004	/* desactivado localmente */

Este indicador se establece en jupes globales que se han desactivado
localmente. Este indicador es mantenido internamente por el subsistema
jupe.
</macro>

<struct>
struct Jupe;

La estructura Jupe describe todo acerca de una jupe dada. La aplicación no
puede acceder directamente a ninguno de sus campos; Utilice las funciones
y macros que se describen a continuación.
</struct>

<function>
int JupeIsActive(struct Jupe* j);

Esta macro devuelve un valor distinto de cero si el jupe está activo, o 0
en caso contrario. Si un jupe está desactivado localmente, esta macro
siempre devolverá 0.
</function>

<function>
int JupeIsRemActive(struct Jupe* j);

Esta macro devuelve un valor distinto de cero si el jupe está activo,
ignorando si está desactivado localmente o no.
</function>

<function>
int JupeIsLocal(struct Jupe* j);

Esta macro devuelve un valor distinto de cero si el jupe es solo local.
</function>

<function>
char* JupeServer(struct Jupe* j);

Esta macro devuelve el nombre del servidor asociado con el jupe.
</function>

<function>
char* JupeReason(struct Jupe* j);

Esta macro devuelve el motivo que se dio cuando se puso el jupe.
</function>

<function>
time_t JupeLastMod(struct Jupe* j);

Los Jupes tienen un tiempo de modificación que debe ser monótonamente
creciente. Esta macro simplemente devuelve ese tiempo de modificación.
</function>

<function>
int jupe_add(struct Client *cptr, struct Client *sptr, char *server,
	     char *reason, time_t expire, time_t lastmod, unsigned int flags);

Esta función simplemente agrega un jupe, establecido por _sptr_ y con un
_server_, _reason_, _expire_, y _lastmod_ como se especifica. El parámetro
_flags_ es una máscara de bits que consta del OR binario de JUPE_LOCAL
y JUPE_ACTIVE, según corresponda. La función jupe_add() propagará el jupe
a todos los servidores (suponiendo que no se haya pasado JUPE_LOCAL),
y romperá su enlace al servidor especificado por _server_ (suponiendo
que se haya pasado el indicador JUPE_ACTIVE).
</function>

<function>
int jupe_activate(struct Client *cptr, struct Client *sptr, struct Jupe *jupe,
		  time_t lastmod, unsigned int flags);

Esta función activa el jupe especificado por _jupe_, configurando su tiempo
de _lastmod_ como se especifica. Si _flags_ es JUPE_LOCAL y si el jupe está
desactivado localmente, esta función desactivará el indicador de desactivación
local, pero no modificará _lastmod_. Si el jupe está desactivado globalmente,
al pasar esta función, el indicador JUPE_LOCAL no tendrá ningún efecto.
</function>

<function>
int jupe_deactivate(struct Client *cptr, struct Client *sptr,
		    struct Jupe *jupe, time_t lastmod, unsigned int flags);

Esta función es similar a jupe_activate () excepto que desactiva el jupe.
Si el jupe dado es local, entonces el jupe se elimina de la memoria. En
todos los demás casos, el jupe simplemente se desactiva, ya sea localmente
(si JUPE_LOCAL se pasó a través de _flags_) o globalmente. La desactivación
global actualizará el tiempo de _lastmod_.
</function>

<function>
struct Jupe* jupe_find(char *server);

Esta función busca un jupe que coincida con el _server_ dado.
</function>

<function>
void jupe_free(struct Jupe *jupe);

Esta función libera todo el almacenamiento asociado con un jupe dado.
</function>

<function>
void jupe_burst(struct Client *cptr);

Esta función genera una ráfaga (burst) de todos los jupes globales existentes
y los envía al servidor especificado por _cptr_.
</function>

<function>
int jupe_resend(struct Client *cptr, struct Jupe *jupe);

Esta función reenvía el _jupe_ a un servidor especificado por _cptr_.
Se puede usar si, por ejemplo, se descubre que un servidor no está
sincronizado con respecto a un jupe en particular.
</function>

<function>
int jupe_list(struct Client *sptr, char *server);

Esta función envía la información sobre un jupe que coincide con _server_
al cliente especificado por _sptr_. Si _server_ es un puntero NULO, se
envía una lista de todos los jupes.
</function>

<authors>
Kev <klmitch@mit.edu> Original
zoltan <toni@tonigarcia.es> Traducción
</authors>

<changelog>
[2001-6-15 Kev] Documentación inicial de la API jupe.
</changelog>