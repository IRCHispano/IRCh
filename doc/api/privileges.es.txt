El control de acceso se convierte en un problema debido a que cada vez
hay más usuarios que necesitan acceder a ciertas funciones. Tal como está,
ircu tiene solo 3 niveles de acceso: usuario ordinario, operador local y
operador global. Esto no es suficiente control, especialmente sobre algunas
de las funciones más avanzadas y potentes, como las líneas G (G-lines).

Desde la versión u2.10.11, ircu incluye el concepto de privilegios.
Los privilegios son básicamente una cadena de bits arbitrariamente larga.
El acceso a funciones particulares se rige por el valor de un bit particular
de esa cadena de bits; en otras palabras, los privilegios son una forma de 
Lista de Control de Acceso (ACL). Este documento cubre las estructuras básicas
y las macros utilizadas por el sistema de privilegios.

<struct>
struct Privs;

La estructura Privs almacena una cadena de bits de privilegios y representa
el conjunto de privilegios completo de un usuario. Esto se implementa como
una estructura, en lugar de una matriz de enteros, para aprovechar la copia
de la estructura de C.
</struct>

<function>
void PrivSet(struct Privs pset, int priv);

Esta macro establece el privilegio especificado por _priv_ en la estructura
de privilegios. Esta macro evalúa el argumento _priv_ dos veces.
</function>

<function>
void PrivClr(struct Privs pset, int priv);

Esta macro borra el privilegio especificado por _priv_ en la estructura 
de privilegios. Esta macro evalúa el argumento _priv_ dos veces.
</function>

<function>
int PrivHas(struct Privs pset, int priv);

This macro tests whether the privilege specified by _priv_ is set in
the privileges structure, returning non-zero if it is.  This macro
evaluates the _priv_ argument twice.
</function>

<function>
void GrantPriv(struct Client* cli, int priv);

This macro grants a particular client, specified by _cli_, the
privilege specified by _priv_.  This macro evaluates the _priv_
argument twice.
</function>

<function>
void RevokePriv(struct Client* cli, int priv);

This macro revokes the privilege specified by _priv_ from the client.
This macro evaluates the _priv_ argument twice.
</function>

<function>
int HasPriv(struct Client* cli, int priv);

Esta macro comprueba si el privilegio especificado por _priv_ se
establece en la estructura de privilegios, y devuelve un valor
distinto de cero si lo es. Esta macro evalúa el argumento _priv_
dos veces.
</function>

<function>
void client_set_privs(struct Client* client);

El archivo de configuración del ircu aún no admite privilegios. Por
lo tanto, esta función establece los privilegios apropiados para un
operador, según la configuración de varias características (features).
Debe llamarse siempre que haya un cambio en el estado del operador de
IRC de un usuario.
</function>

<function>
int client_report_privs(struct Client *to, struct Client *client);

Esta función envía al cliente especificado por _to_ una lista de los
privilegios que tiene otro cliente. Devuelve un valor de 0 para la
conveniencia de otras funciones que deben devolver un valor entero.
</function>

<authors>
Kev <klmitch@mit.edu> Original
zoltan <toni@tonigarcia.es> Traducción
</authors>

<changelog>
[2001-6-15 Kev] Documentación inicial del sistema de privilegios.
</changelog>
