Estas funciones pretenden ser un reemplazo completo para sprintf y
sprintf_irc. Son una (casi) reimplementación completa y, por supuesto,
son clones de snprintf, lo que dificulta que surjan desbordamientos de
búfer accidentales.

En primer lugar, ¿qué falta? Estas funciones admiten todos los
especificadores de conversión ANSI C y algunos seleccionados de ISO 9x,
con la excepción de todas las conversiones de coma flotante. Las
conversiones de coma flotante son complicadas y probablemente dependerán
de la representación de un número de coma flotante en una arquitectura
particular. Si bien es probable que esa representación se ajuste a algún
estándar, actualmente no se usa en ircu, por lo que pareció una buena
omisión, dada la dificultad de su implementación.

Hay dos cosas más que faltan en esta implementación que serían requeridas
por ANSI; el primero es el soporte para cadenas de caracteres multibyte,
y el segundo es el soporte para locales, ninguno de los cuales tiene relevancia
para ircu, por lo que, de nuevo, la omisión parece ser una buena política.
Además, %#x siempre hace que se imprima '0x' (o '0X'), incluso si el número
es cero.

Estas funciones también tienen algunas extensiones que no se ven en
una implementación compatible con los estándares; técnicamente, las
extensiones ISO 9x entran en esta categoría, por ejemplo. Las extensiones
ISO 9x admitidas son extensiones de tipo, %ju, %tu y %zu; por ejemplo,
%qu y %hhu también son compatibles. Las extensiones agregadas para usar
en ircu son %Tu, que requiere un time_t, y la nueva conversión %C, que
inserta un numérico o un nick, dependiendo del parámetro <dest>. La
extensión GNU %m, que inserta la cadena strerror() correspondiente al
valor actual de errno, también es compatible, al igual que una extensión
especial %v, que esencialmente realiza una llamada recursiva a ircd_snprintf.

La siguiente descripción desciende de la página de manual de Linux para
la familia de funciones printf.

La cadena de formato se compone de cero o más directivas: caracteres
ordinarios (no %), que se copian sin cambios en el flujo de salida; y
especificaciones de conversión, cada una de las cuales da como resultado
la obtención de cero o más argumentos posteriores. Cada especificación
de conversión es introducida por el carácter %. Los argumentos deben
corresponder correctamente (después de la promoción de tipo) con el
especificador de conversión. Después del %, lo siguiente aparece en
secuencia:

* Cero o más de los siguientes indicadores:

  # especificando que el valor debe convertirse a una "forma alternativa".
    Para las conversiones c, d, i, n, p, s y u, esta opción no tiene efecto.
	Para las conversiones o, la precisión del número aumenta para forzar
	el primer carácter de la cadena de salida a cero (excepto si se imprime
	un valor cero con una precisión explícita de cero). Para las conversiones
	de x y X, la cadena '0x' (o '0X' para las conversiones de X) está
	precedida por ella. Para las conversiones e, E, f, g, y G, el resultado
	siempre contendrá un punto decimal, incluso si no hay dígitos que lo
	sigan (normalmente, un punto decimal aparece en los resultados de esas
	conversiones solo si sigue un dígito). Para las conversiones g y G, los
	ceros finales no se eliminan del resultado como de lo contrario. Para
	las conversiones de C, si el destino es local y el origen es un usuario,
	se utiliza el formato nick!user@host.	

  0	especificando relleno de ceros. Para todas las conversiones excepto n,
    el valor convertido se rellena a la izquierda con ceros en lugar de
	espacios en blanco. Si se da una precisión con una conversión numérica
	(d, i, o, u, i, x, y X), se ignora el indicador 0.

  - (un indicador de ancho de campo negativo) indica que el valor
    convertido debe dejarse ajustado en el límite del campo. Excepto
	por n conversiones, el valor convertido se rellena a la derecha
	con espacios en blanco, en lugar de a la izquierda con espacios
	en blanco o ceros. Un - anula un 0 si se dan ambos.

 ' '	(un espacio) que especifica que se debe dejar un espacio en
    blanco antes de un número positivo producido por una conversión de
	signo (d, e, E, f, g, G o i).


  +	especificando que un signo siempre se coloca antes de un número
    producido por una conversión de signo. Una + anula un espacio si
	se utilizan ambos.

  :	especificar que una estructura de Cliente debe ir precedida por
    un carácter ':' si el destino es un usuario.


* Una cadena opcional de dígitos decimales que especifica un ancho de
  campo mínimo. Si el valor convertido tiene menos caracteres que el ancho
  del campo, se rellenará con espacios a la izquierda (o a la derecha, si
  se ha dado el indicador de ajuste a la izquierda) para completar el ancho
  del campo.

* Una precisión opcional, en la forma de un punto ('.') seguido de una
  cadena de dígitos opcional. Si se omite la cadena de dígitos, la
  precisión se toma como cero. Esto da el número mínimo de dígitos que
  aparecen para las conversiones de d, i, o, u, x y X, el número de
  dígitos que aparecerá después del punto decimal para las conversiones
  e, E y f, el número máximo de dígitos significativos para las
  conversiones g y G, o el número máximo de caracteres que se imprimirán
  desde una cadena para las conversiones s.

* El carácter opcional h, que especifica que una siguiente conversión d, i,
  o, u, x o X corresponde a un argumento short int o unsigned short int,
  o que una conversión n siguiente corresponde a un puntero a un argumento
  short int. Si se vuelve a dar el carácter h, se usa char en lugar de
  short int.
 
* El carácter opcional l (ell) que especifica que una siguiente conversión
  d, i, o, u, x, o X se aplica a un puntero a un argumento long int o
  unsigned long int, o que una siguiente n conversión corresponde a un
  puntero a un argumento long int.

* El carácter L que especifica que una siguiente conversión e, E, f, g, o G
  corresponde a un argumento long double, o una conversión siguiente d, i,
  o, u, x, o X corresponde a un argumento long long. Tenga en cuenta que
  long long no se especifica en ANSI C y, por lo tanto, no es portable para
  todas las arquitecturas.

* El carácter opcional q. Esto es equivalente a L.
 
* Un carácter j que especifica que el siguiente entero (d, i, o, u, x,o X)
  la conversión corresponde a un argumento intmax_t.
  
* Un carácter j que especifica que el siguiente entero (d, i, o, u, x,o X)
  la conversión corresponde a un argumento ptrdiff_t.
  
* Un carácter j que especifica que el siguiente entero (d, i, o, u, x,o X)
  la conversión corresponde a un argumento size_t.
  
* Un carácter j que especifica que el siguiente entero (d, i, o, u, x,o X)
  la conversión corresponde a un argumento time_t.

* Un carácter que especifica el tipo de conversión a aplicar.

El ancho o la precisión de un campo, o ambos, se pueden indicar con un
asterisco '*' en lugar de una cadena de dígitos. En este caso, un
argumento int proporciona el ancho o la precisión del campo. Un ancho
de campo negativo se trata como un indicador de ajuste izquierdo seguido
de un ancho de campo positivo; Una precisión negativa se trata como si
faltara.

Los especificadores de conversión y sus significados son:

  diouxX	El argumento int (o variante apropiada) se convierte a
			decimal con signo (d e i), octeto sin signo (o), decimal
			sin signo (u) o notación hexadecimal sin signo (x y X).
			Las letras abcdef se utilizan para x conversiones; las
			letras ABCDEF se utilizan para X conversiones. La precisión,
			si la hay, da el número mínimo de dígitos que deben aparecer;
			si el valor convertido requiere menos dígitos, se rellena a
			la izquierda con ceros.
  
  eE		[NO IMPLEMENTADO] El argumento doble se redondea y se
			convierte en el estilo [-]d.dddedd donde hay un dígito
			antes del carácter de punto decimal y el número de dígitos
			después de que sea igual a la precisión; si falta la precisión,
			se toma como 6; Si la precisión es cero, no aparece ningún
			carácter de punto decimal. Una conversión E usa la letra E
			(en lugar de e) para introducir el exponente. El exponente
			siempre contiene al menos dos dígitos; si el valor es cero,
			el exponente es 00.

  f		[NO IMPLEMENTADO] El argumento doble se redondea y se convierte
		a notación decimal en el estilo [-]ddd.ddd, donde el número de
		dígitos después del carácter de punto decimal es igual a la
		especificación de precisión. Si falta la precisión, se toma como
		6; si la precisión es explícitamente cero, no aparece ningún
		carácter de punto decimal. Si aparece un punto decimal, al menos
		un dígito aparece delante de él.

  g		[NO IMPLEMENTADO] El argumento doble se convierte en el estilo
		f o e (o E para conversiones G). La precisión especifica el número
		de dígitos significativos. Si falta la precisión, se dan 6 dígitos;
		si la precisión es cero, se trata como 1. El estilo e se usa si el
		exponente de su conversión es menor que -4 o mayor o igual que la
		precisión. Los ceros finales se eliminan de la parte fraccionaria
		del resultado; solo aparece un punto decimal si va seguido de al
		menos un dígito.

  c		El argumento int se convierte en un carácter sin signo y el
		carácter resultante se escribe.

  s		Se espera que el argumento "char *" sea un puntero a una matriz de
		tipo de carácter (puntero a una cadena). Los caracteres de la matriz
		se escriben hasta (pero sin incluir) un carácter NUL de terminación;
		si se especifica una precisión, no se escribe más del número
		especificado. Si se da una precisión, no es necesario que esté presente 
		ningún carácter nulo; Si no se especifica la precisión, o si es mayor
		que el tamaño de la matriz, la matriz debe contener un carácter NUL de
		terminación.
  
  p		El argumento del puntero "void *" se imprime en hexadecimal (como por
		%#x o %#lx).
  
  n		El número de caracteres escritos hasta ahora se almacena en el entero
		indicado por el argumento de puntero "int *" (o variante). No se
		convierte ningún argumento.
  
  m		El mensaje de error asociado con el valor actual de errno se imprime
		como por %s.
  
  C		El identificador de argumento del cliente se imprime bajo
		el control del argumento <dest>; si <dest> es NULL o es un
		usuario, se imprime el nombre del cliente (nick o nombre del
		servidor); de lo contrario, se imprime el numérico de red del
		cliente.

  H		Se imprime el argumento identificador del canal (nombre
		del canal).
	
  v		El argumento dado debe ser un puntero a una estructura
		VarData con vd_format y vd_args, deben inicializarse
		adecuadamente. Al regreso de la llamada, vd_chars contendrá
		la cantidad de caracteres agregados al búfer, y vd_overflow
		contendrá la cantidad de caracteres que no se pudieron
		agregar debido al desbordamiento del búfer o debido a una
		precisión.

  %		Se escribe un '%' No se convierte ningún argumento. La
		especificación de conversión completa es '%%'.

En ningún caso, un ancho de campo pequeño o inexistente causa el
truncamiento de un campo; Si el resultado de una conversión es más ancho
que el ancho del campo, el campo se expande para contener el resultado
de la conversión.

<struct>
struct VarData {
  size_t	vd_chars;	/* número de caracteres insertados */
  size_t	vd_overflow;	/* número de caracteres insertados que no pudo ser */
  const char   *vd_format;	/* cadena de formato */
  va_list	vd_args;	/* argumentos para %v */
};

Esta estructura es utilizada por la especificación de conversión %v. El
elemento _vd_format_ debe contener una cadena de formato, y el elemento
_vd_args_ debe ser una lista de argumentos variables. Al regresar de
ircd_snprintf() o ircd_vsnprintf(), el elemento _vd_chars_ contendrá el
número de caracteres que pudieron insertarse, y el elemento _vd_overflow_
contendrá el número de caracteres que no se pudieron insertar.
</struct>

<function>
int ircd_snprintf(struct Client *dest, char *buf, size_t buf_len,
		  const char *format, ...);

Esto formatea la lista de argumentos, bajo el control de _format_, en el
búfer especificado por _buf_, cuyo tamaño se especifica por _buf_len_.
El parámetro _dest_ se usa para determinar si se debe usar un numérico o
un nick para las conversiones de %C.
</function>

<function>
int ircd_vsnprintf(struct Client *dest, char *buf, size_t buf_len,
		   const char *format, va_list args);

Esta función es idéntica a la función ircd_snprintf() excepto por la
lista de argumentos variables dada por _args_.
</function>

<authors>
Kev <klmitch@mit.edu> Original
zoltan <toni@tonigarcia.es> Traducción
</authors>

<changelog>
[2001-6-15 Kev] Documentación inicial de la familia de funciones
ircd_snprintf.
</changelog>