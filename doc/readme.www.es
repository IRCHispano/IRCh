Más, y actualizada, la información se puede recuperar de las siguientes
páginas web:

* Proyecto de Documentos de Undernet:
		http://www.user-com.undernet.org/documents/

* Notas de la versión y repositorio de código fuente:
		http://coder-com.undernet.org/

* Scripts ircII para soportar las extensiones undernet:
		http://coder-com.undernet.org/ircii/

* Política de Uso de Undernet:
		http://www.user-com.undernet.org/documents/aup.html

* Etiqueta del Operador:
		http://www.user-com.undernet.org/documents/operman.html

* Nuevos links de servidores y info de enrutamiento:
		http://routing-com.undernet.org/

* Información sobre un gran número de descriptores de archivos por proceso:
  linux:	http://www.linux.org.za/oskar/patches/kernel/filehandle/
