VISIÓN GENERAL
==============

El mecanismo de extensión de consulta proporciona un medio por el
cual los servidores puede enviar consultas a otros servidores y
recibir respuestas.  Obviamente los servidores ircu ordinarios no
tienen la necesidad de este mecanismo, pero permite que los servicios
pseudo-servidor puedan usar para comunicarse entre sí.  Adicionalmente,
se han hecho extensiones al protocolo iauth (ver readme.iauth.es) para
permitir a las instancias de iauth enviar y recibir consultas de
extensión.  Esto podría ser utilizado, por ejemplo, para mandar información
del cliente para un escaneo de proxy inmediato por un servicio centralizado,
o para consultar una base de datos centralizado para parámetros de inicio
de sesión.

DESCRIPCIÓN DETALLADA
=====================

El mencanismo de extensión de consulta consiste en un par de comandos,
el comando XQUERY (token XQ) y el comando XREPLY (token XR).  Los servidores
y los Operadores de IRC pueden enviar un XQUERY, nombrando un servicio de
destino, un token opaco de "enrutamiento", y la consulta; se espera al
servicio de destino para responder con un mensaje XREPLY, que incluirá el
token de enrutamiento de la consulta y respuesta del servicio a la consulta.

La sintaxis de la consulta es:

  <prefix> XQ <target> <routing> :<query>

donde <target> es el numérico del servicio de destino, <routing> es
el token opaco de "enrutamiento", y <query> es la consulta para el
servicio que responderá.  Los Operadores de IRC también pueden emitir
consultas, usando el comando XQUERY con los mismos parámetros, con
<target> permitido para ser una máscara de nombre de servidor; esto es
en gran parte destino a fines de depuración.  Los usuarios normales no
pueden emitir comandos XQUERY, con el fin de fomentar el uso de los
comandos estándar PRIVMSG y NOTICE.

La sintaxis de respuesta es:

  <prefix> XR <target> <routing> :<reply>

donde <target> es el origen de la consulta original, <routing> es el 
token opaco de "enrutamiento" de la consulta, y <reply> es la respuesta
del servicio a la consulta.  Este comando solo puede ser emitido por
los servidores.

USO CON IAUTH
=============

Se han realizado tres extensiones de mensaje al protocolo iauth.  Una
instancia iauth puede emitir un XQUERY mediante el uso del mensaje de
cliente "X" con la siguiente sintaxis:

  X <servername> <routing> :<query>

Si <servername> no está actualmente vinculado a la red, ircu responderá
con un mensaje de servidor "x", con la siguiente sintaxis:

  <id> x <servername> <routing> :Server not online

Si, por otro lado, <servername> nombra un servidor on-line válido, ircu
antepondrá "iauth:" al token de "enrutamiento" y reenviará la consulta a
ese servidor.  Si se recibe un REPLY del servicio, ircu eliminará el
prefijo "iauth:" en el token de "enrutamiento" y enviará la respuesta
a la instancia iauth con el mensaje de servidor "X":

  <id> X <servername> <routing> :<reply>

Tener el prefijo "iauth:" en el token de "enrutamiento" habilita al ircu
futuras extensiones que deseen utilizar el mecanismo para ser diferenciadas
de las extensiones de consulta originadas de iauth.

FUNDAMENTO
==========

El mecanismo de extensión de consulta se originó como parte de un
esfuerzo para establecer un sistema confiable de inicio de sesión
para conectar a Undernet.  Intentos ateriores de tal sistema requerían
conexiones paralelas fuera de banda, y podría dar lugar a un compromiso
de IPs ocultas (como la IP del servidor de base de datos de X).  Además,
sin las extensiones extensas a GNUWorld, ciertas restricciones de inicio
de sesión --como el  contador de máximo de clientes logueados-- no se
pudo hacer cumplir de manera confiable.  Al proporcionar un mecanismo
de señalización que iauth puede hacer uso directo, estos problemas se
eliminan; el único problema que queda es qué hacer si auth no puede
comunicar con el servidor de inicio de sesión, que se puede resolver
a través de decisiones poñiticas y tiempos de espera implementados
dentro de la instancia iauth.

La razón para el token opaco de "enrutamiento" es proporcionar
emparejamiento entre respuestas y consultas.  La falta de esa pareja
es uno de los defectos del protocolo IRC, como se especifica en el
RFC 1459; solo una extensión de Undernet ha intentado proporcionar
tal emparejamiento --una extensión poca utilizada para el comando /WHO.
En el un contexto de iauth, tal emparejamiento es crítico; de lo
contrario, iauth potencialmente podría aplicar una respuesta al
cliente equivocdo.  Aunque el emparejamiento podria ser parte de la
consulta, tiene sentido hacerlo parte del mensaje del protocolo base,
haciéndolo explícito.  Esto también permite al ircu agregar datos de
enrutamiento al token, haciendo posible que más extensones que solo
iauth haga uso de las extensiones de consulta.
