Si deseas sangrar (indent) este archivo fuente, para convertir
el código fuente para el estilo de programación utilizado, debe
utilizar `make indent' en el directorio base.

Para que esto funcione, necesitas tener una version de `indent'  2.1.0
o superior en tu PATH.  GNU indent 2.1.0 está disponible en todos los
sitios de GNU, su sitio FTP principal es  ftp://ftp.gnu.org/indent/.
O puedes descargar directamente desde la página Web de su mantenedor en
http://www.xs4all.nl/~carlo17/indent/
